// Bất đồng bộ
setTimeout(function () {
  console.log(4);
}, 3000);

//Đồng bộ
console.log(1);
console.log(2);
console.log(3);
// Xử lý đồng bộ trước, r mới tới bất đồng bộ dc xử lý (theo thời gian mili giây dc đặt trong đó ko qtrong thứ tự đc đặt)

//pending, success, fail ~ catch
var promise = axios({
  url: "https://635ff9213e8f65f283c08516.mockapi.io/todos",
  method: "GET",
});
promise
  .then(function (res) {
    console.log("res: ", res);
  })
  .catch(function (err) {
    console.log("err: ", err);
  });
