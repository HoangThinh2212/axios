const BASE_URL = "https://635ff9213e8f65f283c08516.mockapi.io";
// thêm biến idEdit để dùng khi chỉnh sửa id
var idEdited = null;

function fetchAllTodo() {
  //render all todos services
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    //Nếu thành công ~ dùng .then
    .then(function (res) {
      // console.log("res todos: ", res.data);
      turnOffLoading();
      renderTodoList(res.data);
    })
    // Nếu thất bại ~ dùng .catch
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
// chạy lần đầu khi load trang
fetchAllTodo();

//Remove all todos
function removeTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      // gọi lại API get all todos
      fetchAllTodo();
      console.log("res: ", res);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

// Add Todos
function addTodo() {
  var data = layThongTinTuForm();

  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: false, // biến mới khi thêm vào sẽ là chưa hoàn thành
  };
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    //Nếu thành công ~ dùng .then
    .then(function (res) {
      turnOffLoading();
      // thêm vào để clear giá trị sau khi thêm giá trị vào
      document.getElementById("name").value = "";
      document.getElementById("desc").value = "";

      // console.log("res todos: ", res.data);
      fetchAllTodo();
      console.log("res: ", res);
    })
    // Nếu thất bại ~ dùng .catch
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

// Edit Todos
function editTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      // Show thông tin lên Form
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;

      idEdited = res.data.id;
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

// Update Todos
function updateTodo() {
  turnOnLoading();
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      console.log("res: ", res);

      turnOffLoading();
      fetchAllTodo();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
